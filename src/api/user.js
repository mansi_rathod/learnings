var AWS = require('aws-sdk');
AWS.config.update({
    region: "ap-south-1",
    endpoint: "http://localhost:8001"
});
var docClient = new AWS.DynamoDB.DocumentClient();
var table = "to-do";
var username2 = "Anyone2";
var task2 = " ";
var params = {
    TableName: table,
    Key: {
        "username": username2,
        "task": task2
    }
};
console.log("Adding a new item...");
docClient.get(params, function (err, data) {
    if (err) {
        console.error("Unable to add item. Error JSON:", JSON.stringify(err, null, 2));
    }
    else {
        console.log("Added item:", JSON.stringify(data, null, 2));
    }
});
