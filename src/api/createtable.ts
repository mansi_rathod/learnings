import { table } from "console";
import { checkAndCreateUser } from "./user";

var AWS = require("aws-sdk");

AWS.config.update({
  region: "ap-south-1",
  endpoint: "http://localhost:8000"
});
var docClient = new AWS.DynamoDB.DocumentClient();
applicationCache.get('/user/:userid',(req,res)=>{
    var dynamodb = new AWS.DynamoDB();
 var params = {
    TableName : "to-do",
    KeySchema: [
        { AttributeName: "username", KeyType: "HASH"},
        { AttributeName: "task", KeyType: "RANGE"}
    ],
    AttributeDefinitions: [
        { AttributeName: "username", AttributeType: "S" },
        { AttributeName: "task", AttributeType: "S" },
    ],
    ProvisionedThroughput: {
        ReadCapacityUnits: 5,
        WriteCapacityUnits: 5
    }
};


//old method
export const checkAndCreateTableDontDo0 = (username: string, task :string): void => {
    docClient.get(params, function (err, data) {
            if (err) {
            console.error("Unable to create table. Error JSON:", JSON.stringify(err, null, 2) );
            } else if (data == null)
            {
                console.log("creating table")
                dynamodb.createTable(params, function(err, data) {
                    if (err) {
                        console.error("Unable to create table. Error JSON:", JSON.stringify(err, null, 2));
                    } else {
                        console.log("Created table. Table description JSON:", JSON.stringify(data, null, 2));
                    }
                })
            }
            else{
                console.log("Table exist")
            }
        
        })   
}
//little old
export const checkAndCreateTableDontDo = (username: string, task :string): Promise<boolean> => {
    return docClient.get(params).then(data => {
        if(data)
        {
            console.log("Table exist");
            return true;
        }

        console.log("creating table");

        return dynamodb.createTable(params).then(data => {
            console.log("Created table. Table description JSON:", JSON.stringify(data, null, 2));
            return true;
        }).catch(error => {
            console.error("Unable to create table. Error JSON:", JSON.stringify(error, null, 2));
            return false;
        });
    }).catch(error => {
        console.error("Unable to create table. Error JSON:", JSON.stringify(error, null, 2) );
        return false;
    })
}

// chaining promises
export const checkAndCreateTableDontDo2 = async (username: string, task :string): Promise<boolean> => {
    // get table
    // if not exist create table
   return true;
}
