import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';


class MyForm extends React.Component {
  constructor(props) {
    super(props);
    this.state = { username: '' };
  }
  mySubmitHandler = (event) => {
    event.preventDefault();
    alert("You are submitting " + this.state.username);
  }
  myChangeHandler = (event) => {
    this.setState({username: event.target.value});
  }
  render() {
    return (
      <form onSubmit={this.mySubmitHandler}>

     <div className="abc"><h1><center>Welcome to TODO App</center></h1> 
      <p>Provide your Username:</p>
     <center><input
        type='text'
        onChange={this.myChangeHandler}
      /></center> 
     <center><input
        type='submit'
      /></center> </div>
      </form>
    );
  }
}

ReactDOM.render(<MyForm />, document.getElementById('root'));
 
 
 

